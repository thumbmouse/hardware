# Description

thumbMouse is a finger-mounted bluetooth pointer device, based on BlackBerry trackpad, for avoiding shoulder/wrist pain from mouse/laptop touchpad use

# License

The contents of this repository, unless otherwise noted, are offered under the terms of the CC-BY-SA 4.0 license, as Open-Source Hardware (OSHW), according to the definition found in https://www.oshwa.org/definition/

